FROM maven:3.6.1-amazoncorretto-11 AS builder

WORKDIR /app

ADD pom.xml .
RUN ["mvn", "verify", "dependency:resolve"]

ADD src/ src/
RUN ["mvn", "compile", "test", "package"]


FROM amazoncorretto:11

WORKDIR /app


COPY --from=builder /app/target/app-jar-with-dependencies.jar app.jar

EXPOSE 8080
CMD ["java", "-jar", "app.jar"]