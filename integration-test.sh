#!/bin/bash

# Give the app time to start up
sleep 20

value=$(curl http://$TEST_CONTAINER:8080/hello)
expected="Hello World!"

if [ "$value" == "$expected" ]; then
    echo "Passed the integration test."
    true
else
    echo "Failed the integration test. Expected '$expected', got '$value' instead."
    false
fi