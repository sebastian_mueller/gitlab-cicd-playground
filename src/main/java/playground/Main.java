package playground;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import static spark.Spark.*;

public class Main {

    static String computeGet() {
        return "Hello World!";

    }

    static String computePost(String body) {
        String[] components = body.split(" ");

        String name = components[0];
        int age = Integer.parseInt(components[1]);

        return String.format("Hi! Your name is %s and you are %d years old.", name, age);
    }

    public static void main(String[] args) {

        port(8080);

        get("/hello", (request, response) -> computeGet());

        post("/hello", (request, response) -> {
            JSONParser jsonParser = new JSONParser();
            JSONObject jsonObject = (JSONObject) jsonParser.parse(request.body());
            String name = (String) jsonObject.get("name");
            String age = (String) jsonObject.get("age");
            String body = name.concat(", ").concat(age);
            return computePost(body);
        });
    }
}
