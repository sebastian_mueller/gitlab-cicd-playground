package playground;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class TestHallo {

    @Test
    public void testGet() {
        assertEquals(Main.computeGet(), "Hello World!");
        assertNotEquals(Main.computeGet(), "Bye");
    }

    @Test
    public void testPost() {
        assertEquals(Main.computePost("Kevin 12"), "Hi! Your name is Kevin and you are 12 years old.");
    }
}
